import os
import sys
import site
import cherrypy

crys_api_path = os.path.dirname(
    os.path.dirname(  # crys full path
        __file__  # crys\scripts\run_server.py
    )
)
site.addsitedir(crys_api_path)


from crys.application import app
import crys.constants as constants


def run_server():
    try:
        port = int(sys.argv[1])
    except Exception:
        port = constants.SERVER['PORT']

    cherrypy.tree.graft(app, '/')
    cherrypy.config.update({
        'server.socket_host': constants.SERVER['HOST'],
        'server.socket_port': port,
        'server.thread_pool': 50,
        'engine.autoreload.on': False
    })

    cherrypy.engine.signals.subscribe()

    try:
        cherrypy.engine.start()
    except KeyboardInterrupt:
        cherrypy.engine.stop()


if __name__ == "__main__":
    run_server()
