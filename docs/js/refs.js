let refs = {
  HTML: `<dd property="schema:citation" typeof="schema:WebPage"
      resource="http://www.w3.org/TR/html5/">
    <cite property="schema:name"><a href="http://www.w3.org/TR/html5/">One of the HTML
    Specifications</a></cite>.
  </dd>
  `,
  DOACC: `
  <dd property="schema:citation" typeof="schema:WebPage" resource="https://doacc.github.io/index.html">
    <cite property="schema:name"><a href="https://doacc.github.io/index.html">Description of A CryptoCurrency</a></cite>
  </dd>
  `
};
export default refs;
