from crys.application.utils import retrieve_object_for_property


class SPARQLQueryCreator:
    @staticmethod
    def field_to_predicate(field):
        simple_fields = {
            'block-reward', 'block-time', 'date-founded',
            'distribution-scheme', 'incept', 'maturation',
            'pos', 'pow', 'premine', 'protection-scheme',
            'protocol', 'retarget-time', 'source', 'total-coins',
            'prefLabel', 'reward-modifier', 'comment', 'block_reward',
            'coin', 'release', 'repository', 'powscheme'
        }
        mapping = dict()
        for f in simple_fields:
            mapping[f] = 'doacc:' + f

        return mapping[field]

    @staticmethod
    def create(filters):
        where_condition = ''

        for i, filter_ in enumerate(filters):
            field = filter_['field'].replace('-', '_')
            predicate = SPARQLQueryCreator.field_to_predicate(filter_['field'])

            if predicate in {'doacc:protocol', 'doacc:pow', 'doacc:pos', 'doacc:distribution-scheme'}:
                if not isinstance(filter_['value'], list):
                    v = [filter_['value']]
                else:
                    v = filter_['value']

                values = [
                    retrieve_object_for_property(predicate, item)
                    for item in v
                ]
                values = v

                if None in values:
                    values.remove(None)

                clause = '?coin {predicate} ?{value} .\n' \
                         '?{value} rdfs:label ?{field} .\n'.format(
                            predicate=predicate,
                            value=field + '_whatever',
                            field=field
                         )

                if filter_['operator'].lower() == 'not in':
                    str_filter = 'FILTER (LCASE(STR(?{field})) NOT IN ({values})) \n'.format(
                        field=field,
                        values=', '.join({'"{}"'.format(item) for item in values})
                    )
                else:
                    str_filter = 'FILTER (LCASE(STR(?{field})) IN ({values})) \n'.format(
                        field=field,
                        values=', '.join({'"{}"'.format(item) for item in values})
                    )
            elif predicate in {'doacc:date-founded'}:
                clause = '?coin doacc:date-founded ?{field} .\n'.format(
                            predicate=predicate,
                            field=field
                         )
                str_filter = 'FILTER (?{field} {operator} "{value}"^^xsd:date) \n'.format(
                    field=field,
                    operator=filter_['operator'],
                    value=filter_['value']
                )
            else:
                value = filter_['value']

                clause = '?coin {} ?{} .\n'.format(
                    predicate, field
                )
                if filter_['operator'] in {'<', '>'}:
                    str_filter = 'FILTER (xsd:float(?{}) {} {})\n'.format(
                        field, filter_['operator'], value
                    )
                elif filter_['operator'] == '=':
                    try:
                        if value.lower() == "nan":
                            raise Exception('not processing nan')
                        value = float(value)
                        str_filter = 'FILTER (xsd:float(?{}) {} {})\n'.format(
                            field, filter_['operator'], value
                        )
                    except Exception:
                        str_filter = 'FILTER (LCASE(STR(?{})) {} {})\n'.format(
                            field, filter_['operator'], str(value).lower()
                        )
                elif filter_['operator'] in ['in', 'not in']:
                    str_filter = 'FILTER (?{} {} {})\n'.format(
                        field, filter_['operator'].upper(), '({})'.format(', '.join(value))
                    )
                else:
                    str_filter = '\n'

            where_condition = where_condition + clause + str_filter

        query = 'SELECT DISTINCT ?coin ' \
                + ' '.join({'?' + filter_['field'].replace('-', '_') for filter_ in filters}) \
                + '\n' \
                + 'WHERE {\n' \
                + '?coin a doacc:Cryptocurrency .\n' \
                + where_condition + '}\n' \
                + 'LIMIT 1000'

        return query


if __name__ == '__main__':

    # res = SPARQLQueryCreator.retrieve_object_for_property(
    #     'doacc:protocol',
    #     'bitcoin'
    # )
    # print(res)
    #
    # raise SystemExit

    filters = [
        {
            'operator': '>',
            'field': 'block-reward',
            'value': 300
        },
        {
            'operator': '<',
            'field': 'block-reward',
            'value': 1500
        }
    ]

    filters = [
        {
            'operator': '=',
            'field': 'protocol',
            'value': 'bitcoin'
        },
        {
            'operator': '>',
            'field': 'block-reward',
            'value': 500
        }
    ]

    res = SPARQLQueryCreator.create(filters)
    print(res)
