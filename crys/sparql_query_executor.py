from SPARQLWrapper import SPARQLWrapper, JSON, POST, POSTDIRECTLY, TSV

import crys.constants as constants


class SPARQLQueryExecutor:
    def query(self, query=None):
        if not query:
            query = """
            SELECT ?coin
            WHERE {
                ?coin a doacc:Cryptocurrency .
            }
            LIMIT 10
            """

        sparql = SPARQLWrapper(constants.SPARQL_ENDPOINT_QUERY_URL)
        sparql.setMethod(POST)
        sparql.setRequestMethod(POSTDIRECTLY)

        sparql.setQuery(
            """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX doacc: <http://purl.org/net/bel-epa/doacc#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dc:  <http://purl.org/dc/elements/1.1/>
            PREFIX cce: <http://purl.org/net/bel-epa/cce#>
            PREFIX geo: <http://www.fao.org/countryprofiles/geoinfo/geopolitical/resource/>
            {}
            """.format(
                query
            )
        )

        sparql.setReturnFormat(JSON)
        sparql.setCredentials(
            user=constants.SPARQL_ENDPOINT_USER,
            passwd=constants.SPARQL_ENDPOINT_PASSWORD
        )

        results = sparql.query()
        results = results.convert()

        return results['results']['bindings']

    def query_tab_separated_output(self, query):
        sparql = SPARQLWrapper(constants.SPARQL_ENDPOINT_QUERY_URL)
        sparql.setMethod(POST)
        sparql.setRequestMethod(POSTDIRECTLY)

        sparql.setQuery(
            """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX doacc: <http://purl.org/net/bel-epa/doacc#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dc:  <http://purl.org/dc/elements/1.1/>
            PREFIX cce: <http://purl.org/net/bel-epa/cce#>
            PREFIX geo: <http://www.fao.org/countryprofiles/geoinfo/geopolitical/resource/>
            {}
            """.format(
                query
            )
        )

        sparql.setReturnFormat(TSV)
        sparql.setCredentials(
            user=constants.SPARQL_ENDPOINT_USER,
            passwd=constants.SPARQL_ENDPOINT_PASSWORD
        )

        results = sparql.query()
        results = results.convert()

        return results.decode()

    def update_query(self, query):
        sparql = SPARQLWrapper(constants.SPARQL_ENDPOINT_UPDATE_URL)
        sparql.setMethod(POST)
        sparql.setRequestMethod(POSTDIRECTLY)

        sparql.setQuery(
            """
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX doacc: <http://purl.org/net/bel-epa/doacc#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX  dc:   <http://purl.org/dc/elements/1.1/>
            PREFIX cce: <http://purl.org/net/bel-epa/cce#>
            PREFIX geo: <http://www.fao.org/countryprofiles/geoinfo/geopolitical/resource/>
            {}
            """.format(
                query
            )
        )

        sparql.setReturnFormat(JSON)
        sparql.setMethod(POST)
        sparql.setRequestMethod(POSTDIRECTLY)
        sparql.setCredentials(
            user=constants.SPARQL_ENDPOINT_USER,
            passwd=constants.SPARQL_ENDPOINT_PASSWORD
        )

        sparql.query().convert()

        return True
