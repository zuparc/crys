__author__ = 'Robert-Mihail UNGUREANU <roungureanu@bitdefender.com>'

import jwt
import time

from functools import wraps

from flask import request

from crys.constants import SECRET
from crys.application.exceptions import InvalidRequest


def requires_authorization(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        auth_token = request.headers.get('AuthToken')
        if not auth_token:
            raise InvalidRequest(
                message='Not authenticated.',
                status_code=401
            )

        token = jwt.decode(auth_token, SECRET)
        is_authorized = token['expiry'] <= time.time()

        if is_authorized:
            raise InvalidRequest(
                message='Access Forbiddeen!',
                status_code=403
            )

        return func(*args, **kwargs)

    return wrapper
