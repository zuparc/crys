import uuid
import time

import jwt

from crys.constants import SECRET
from crys.sparql_query_executor import SPARQLQueryExecutor


def generate_token(expiry=None):
    if not expiry:
        expiry = time.time() + 7 * 24 * 3600

    token = {
        'random': uuid.uuid4().hex,
        'expiry': expiry
    }

    return jwt.encode(token, key=SECRET)


def build_coins_query(coin_ids):
    query = '''
        SELECT ?coin ?property ?value ?description 
        WHERE {{
                ?coin a doacc:Cryptocurrency ;
                   ?property ?value .
            OPTIONAL {{ ?value rdfs:label ?description}}
            FILTER (?coin IN ({}))
        }}
    '''.format(
        ', '.join(
            'doacc:' + coin_id
            for coin_id in coin_ids
        )
    )

    return query


def retrieve_coins(coin_ids):
    query = build_coins_query(coin_ids)

    query_result = SPARQLQueryExecutor().query(query)

    coins = dict()

    for entry in query_result:
        coin_uri = entry['coin']['value']
        coin_id = coin_uri.rsplit('#', 1)[-1]

        property_uri = entry['property']['value']
        items = property_uri.rsplit('#', 1)
        if len(items) == 1:
            items = property_uri.rsplit('/', 1)
        property_ = items[-1]

        if entry.get('description', dict()).get('value'):
            value = entry['description']['value']
        else:
            value = entry['value']['value']

        try:
            value = int(value)
        except Exception:
            try:
                if value != "nan":
                    value = float(value)
            except Exception:
                if value.lower() in {"true", "false"}:
                    value = bool(value)

        if property_ != 'type':
            if coin_id not in coins:
                coins[coin_id] = dict()
            coins[coin_id][property_] = value

    for key in coins:
        if 'coin' in coins[key]:
            coins[key]['id'] = coins[key].pop('coin').rsplit('#', 1)[-1]
        else:
            coins[key]['id'] = key

    return list(coins.values())


def retrieve_object_for_property(property_, label):
    query = '''
        SELECT DISTINCT ?id
        WHERE {{
            ?s {} ?id .
            ?id rdfs:label ?label .
            FILTER (str(?label) = "{}")
        }}
    '''.format(property_, label)

    result = SPARQLQueryExecutor().query(query)
    if result:
        return result[0]['id']['value']
    else:
        return None


def retrieve_possible_values_for_property(property_):
    query = '''
        SELECT DISTINCT ?label
        WHERE {{
            ?s {} ?id .
            ?id rdfs:label ?label .
        }}
    '''.format(property_)

    result = SPARQLQueryExecutor().query(query)

    return [entry['label']['value'] for entry in result]


def to_rdf_coins(coins):
    pass


def to_json_ld(coins):
    pass


if __name__ == '__main__':
#     TOKEN_EXAMPLE = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyYW5kb20iOiI5YWFjMTE3OTVkN2I0NGExYmU0M2Y2ZWI4ZDdiZjc2NiIsImV4cGlyeSI6MTU4MDMwOTk4Mi40MjYwOTA1fQ.L9XLE_APkhl7ZB7MMJ1qAAnVKQLpM7UComE_jhvTrWk'
    print(generate_token().decode())
