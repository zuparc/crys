import collections
import inspect


def group_by_intervals(data, intervals=5):
    smallest = min(data)
    biggest = max(data)
    biggest = 1000000000 if biggest > 1000000000 else biggest

    delta = biggest - smallest
    step = delta // intervals

    s = list(range(smallest, biggest + step, step))
    first = s[0]
    to_return = list()
    for i in s[1:]:
        to_return.append((first, i))
        first = i

    return to_return


class PluginManager:
    def __init__(self):
        pass

    @classmethod
    def plugin_group_by_total_coins(cls, data):
        data = [
            {i: j if isinstance(j, str) else str(j) for i, j in item.items()}
            for item in data
        ]

        filtered_data = [i for i in data if
                         i.get("total-coins", "").isnumeric() and int(i.get(
                             "total-coins")) < 100000000]
        all_coins = [int(i['total-coins']) for i in filtered_data]

        intervals = group_by_intervals(all_coins, intervals=8)

        response = dict()
        for item in filtered_data:
            total_coins = int(item['total-coins'])
            for interval in intervals:
                start, finish = interval
                key = 'Between {} - {}'.format(start, finish)
                if start <= total_coins <= finish:
                    response[key] = response.get(key, 0) + 1
                    break
            else:
                # response["TOO_BIG"].append(item['prefLabel'])
                response["TOO_BIG"] = response.get("TOO_BIG", 0) + 1

        return {
            'type': "pie",
            'layout': {
                'title': "Grouped by Total Coins",
            },
            'data': [{
                'labels': [i for i in response.keys()],
                'values': list(response.values()),
                'orientation': "v",
            }]
        }

    @classmethod
    def plugin_group_by_block_reward(cls, data):
        data = [
            {i: j if isinstance(j, str) else str(j) for i, j in item.items()}
            for item in data
        ]
        field = "block-reward"
        filtered_data = [i for i in data if i.get(field, "").isnumeric()]

        intervals = group_by_intervals(
            [int(item[field]) for item in filtered_data],
            intervals=8
        )

        response = dict()
        for item in filtered_data:
            value = int(item[field])
            for start, finish in intervals:
                key = 'Between {} - {}'.format(start, finish)

                if start <= value <= finish:
                    response[key] = response.get(key, 0) + 1
                    break
            else:
                response["TOO_BIG"] = response.get("TOO_BIG", 0) + 1

        return {
            'type': "pie",
            'layout': {
                'title': "Grouped by Block Reward",
            },
            'data': [{
                'labels': [i for i in response.keys()],
                'values': list(response.values()),
                'orientation': "v",
            }]
        }

    @classmethod
    def plugin_group_by_founded_year(cls, data):
        data = [
            {i: j if isinstance(j, str) else str(j) for i, j in item.items()}
            for item in data
        ]
        field = "date-founded"
        filtered_data = [i for i in data if i.get(field)]

        response = dict()
        for item in filtered_data:
            value = item[field]
            key = value.split("-")[0].split("_")[0]
            response[key] = response.get(key, 0) + 1

        # return {
        #     'type': 'date-chart',
        #     'labels': list(response.keys()),
        #     'data': list(response.values()),
        #     'title': "Grouped by Founded Year"
        # }

        return {
            'type': "bar",
            'layout': {
                'title': "Grouped by Founded Year",
            },
            'data': [{
                'x': [int(i) for i in response.keys()],
                'y': list(response.values()),
                'orientation': "v",
            }]
        }

    @classmethod
    def plugin_grouped_by_maturity(cls, coins):
        coins = [
            {i: j if isinstance(j, str) else str(j) for i, j in item.items()}
            for item in coins
        ]

        def checker(x):
            try:
                if int(x['total-coins']) < 100000000 \
                        and int(x['maturation']) and x['premine'] != "non":
                    return True
            except:
                pass

            return False

        def mapper(x):
            try:
                if "%" in x['premine']:
                    premine = float(x['premine'].split("%")[0])
                    if premine > 100 or premine > 0:
                        return None
                    x['premine'] = (premine * x["total-coins"]) // 100

                if float(x['premine']) >= int(x['total-coins']):
                    return None
                return float(x['premine']), int(x['total-coins']), \
                       int(x['maturation']), x['prefLabel']
            except Exception as e:
                # print(e, x)
                return None

        filtered = list(filter(checker, coins))
        mapped = [i for i in map(mapper, filtered) if i]
        mapped = mapped[:25]

        return {
                'type': "bar",
                'layout': {
                    'title': "Coins maturity and premine considering total coins",
                },
                'data': [
                    {
                        "x": [i[3] for i in mapped],
                        "y": [100 for _ in mapped],
                        "name": "Total Coins"
                    },
                    {
                        "x": [i[3] for i in mapped],
                        "y": [i[0] * 100 / i[1] for i in mapped],
                        "name": "Premined"
                    },
                    {
                        "x": [i[3] for i in mapped],
                        "y": [-i[2] for i in mapped],
                        "name": "Maturity"
                    },
                ]
            }


    @classmethod
    def plugin_table(cls, coins):
        coins = [
            {i: j if isinstance(j, str) else str(j) for i, j in item.items()}
            for item in coins
        ]

        headers = [
            ('prefLabel', "Label"),
            ('symbol', "Symbol"),
            ('date-founded', "Date Founded"),
            ('block-reward', "Block Reward"),
            ('total-coins', "Total Coins"),
            ('maturation', "Maturation"),
            ('description', "Description"),
        ]

        floats = ("block-reward", "total-coins", "maturation")

        def checker(x):
            try:
                for i in headers:
                    c = x[i[0]]  # checks if exists

                if float(x["block-reward"]) \
                        and float(x["block-time"]) \
                        and int(x['total-coins']) < 100000000 \
                        and float(x["block-reward"]) < 2500 \
                        and x['prefLabel']:
                    return True
            except:
                pass

            return False

        filtered = list(filter(checker, coins))
        filtered = filtered[:15]

        """
        label,symbol,pow,block reward, block time, total coins, maturation, date-founded, description
        """
        lheaders = [i[0] for i in headers]
        return {
            'type': 'table',
            'layout': {
                'title': "Most recent 30 Cryptocurrencies",
            },
            'data': [[i[1] for i in headers]] +
                    [[i[h] if h not in floats else float(i[h]) for h in
                      lheaders]
                     for i in filtered]
        }
    @classmethod
    def plugin_predef2(cls, coins):
        coins = [
            {i: j if isinstance(j, str) else str(j) for i, j in item.items()}
            for item in coins
        ]

        def checker(x):
            try:
                if float(x["block-reward"]) \
                        and float(x["block-time"]) \
                        and int(x['total-coins']) < 100000000 \
                        and float(x["block-reward"]) < 2500:
                    return True
            except:
                pass

            return False

        filtered = filter(checker, coins)
        coins = list(sorted(filtered, key=lambda x: float(x['block-time'])))
        coins = coins[:20]

        br = [float(x['block-reward']) for x in coins]
        tc = [float(x['total-coins']) for x in coins]
        max_tc = max(tc)
        tc = [i * 100 / max_tc for i in tc]
        labels = [x['prefLabel'] for x in coins]

        return {
                'type': "bubble",
                'layout': {
                    'title': "Cryptocurrencies ordered by block-reward with respect to total coins",
                },
                'data': [{
                    'x': labels,
                    'y': br,
                    'mode': 'markers',
                    'marker': {
                        'size': tc
                    }
                }]
            }

    @classmethod
    def get_all_plugins(cls):
        all_p = inspect.getmembers(PluginManager, predicate=inspect.ismethod)
        return [i[0] for i in all_p if i[0].startswith("plugin_")]


REGISTERED_PLUGINS = PluginManager.get_all_plugins()
