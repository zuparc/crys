var elemCount = 0;

function getNewMain(rm=true) {
    var father = document.getElementById("allmain")
    if (rm) {
        var main = document.getElementById("main")
        main.removeAttribute("id")
    }
    var newmain = document.createElement("div")
    newmain.setAttribute("id", "main")
    father.appendChild(newmain)
    return newmain
}

function write(elem, overwrite=false) {
    // overwrite = false => append
    if (overwrite) {
        document.getElementById("allmain").innerHTML = ""
        getNewMain()
    }
    var main = document.getElementById("main")
    main.innerHTML = document.getElementById("buttons").innerHTML
    main.appendChild(elem)
    getNewMain()
    $('table').tablesort()
}

// usage: write(getTable(testdata))

var testdata = [["Name", "Status", "Notes"], ["Jill", "Nada", "None"], ["John", "W/e", "None"], ["Jamie", "who", "maybe"]]
var chartest = [[2478, 5267, 734, 782, 433], ["Africa", "Asia", "Europe", "Latin America", "North America"]];


function getRandomSubarray(arr, size) {
    var shuffled = arr.slice(0), i = arr.length, temp, index;
    while (i--) {
        index = Math.floor((i + 1) * Math.random());
        temp = shuffled[index];
        shuffled[index] = shuffled[i];
        shuffled[i] = temp;
    }
    return shuffled.slice(0, size);
}

var colorList = ['#4dc9f6', '#f67019', '#f53794',
                 '#537bc4', '#acc236', '#166a8f',
                 '#00a950', '#58595b', '#8549ba']
colorList = getRandomSubarray(colorList, colorList.length)

function getColorList(count, randomize=false) {
    if (randomize) {
        return getRandomSubarray(colorList, count)
    }
    return colorList
}


function getTableRow(row, header=false) {
    var elem = "td"
    if (header) elem = "th";
    var tr = document.createElement("tr")
    for (var i = 0; i < row.length; i++) {
        var th = document.createElement(elem)
        if (typeof row[i] != "string" || row[i].length < 46) {
            th.innerHTML = row[i]
        } else {
            var text = document.createElement("textarea")
            text.value = row[i]
            text.readonly = true
            text.style.maxHeight = "100px"
            th.appendChild(text)
            th.classList.add("ui", "form")
        }
        tr.appendChild(th)
    }
    return tr
}


function getTable(data, sortable=true) {
    // data = list of rows, first row = header
    var table = document.createElement("table")
    table.classList.add("ui", "celled", "table")
    if (sortable) {
        table.classList.add("sortable")
    }
    var header = data[0]

    var head = document.createElement("thead")
    head.appendChild(getTableRow(header, true))
    table.appendChild(head)

    var body = document.createElement("tbody")
    for (var i = 1; i < data.length; i++) {
        body.appendChild(getTableRow(data[i]))
    }
    table.appendChild(body)
    return table
}

function getNewCanvas() {
    elemCount += 1
    var canvas = document.createElement("canvas")
    canvas.setAttribute("id", elemCount.toString())
    return canvas
}

function getNewDiv() {
    elemCount += 1
    var div = document.createElement("div")
    div.setAttribute("id", elemCount.toString())
    return div
}

var plotDefaults = {
    "bar": function(data) {
        if(!("marker" in data)) data["marker"] = {}
        if(!("color" in data["marker"])) data["marker"]["color"] = getColorList(2, true)[0]
        data["type"] = "bar"
        return data
    },
    "bubble": function(data) {
        data["mode"] = "markers"
        if(!("marker" in data)) data["marker"] = {}
        var colors = getColorList(data["x"].length, true)
        if(!("color" in data["marker"])) data["marker"]["color"] = colors
        return data
    },
    "pie": function(data) {
        data["type"] = "pie"
        if(!("marker" in data)) data["marker"] = {}
        var colors = getColorList(data["values"].length, true)
        if(!("colors" in data["marker"])) data["marker"]["colors"] = colors
        return data
    }
}
var layouts = {
    "bar": {"barmode": "stack"},
    "bubble": {showlegend: true}
}


function makePlotly(type, data, layout=undefined) {
    if(!layout && type in layouts) {
        layout = layouts[type]
    }
    var div = getNewDiv()
    var mydata = new Array()
    for(var i = 0; i < data.length; i++) {
        mydata.push(plotDefaults[type](data[i]))
    }
    write(div)
    Plotly.newPlot(div.getAttribute("id"), mydata, layout)
}

function getBarPlot(data, layout=layouts["bar"]) {
    makePlotly("bar", data, layout)
}

function getBubblePlot(data, layout=layouts["bubble"]) {
    makePlotly("bubble", data, layout)
}


function getChart(name, data, options) {
    var canvas = getNewCanvas()
    var myChart = new Chart(canvas, {
        type: name,
        data: data,
        options: options
    })
    return canvas
}

function getSimpleChart(name, data, labels, title) {
    // name = type of the chart
    // data, labels = list of elements
    var options = {...Chart.defaults.pie}
    if(title) {
        options.title = Object()
        options.title.display = true
        options.title.text = title
    }
    return getChart(name, {
        datasets: [{
            data: data,
            backgroundColor: getColorList(data.length)
        }],
        labels: labels,
    }, options)
}

function getPieChart(data, labels, title) {
    return getSimpleChart("pie", data, labels, title)
}

function getDoughnutChart(data, labels, title) {
    return getSimpleChart("doughnut", data, labels, title)
}

function getBarChart(data, labels, title) {
    return getSimpleChart("bar", data, labels, title)
}

function getHorizontalBarChart(data, labels, title) {
    return getSimpleChart("horizontalBar", data, labels, title)
}

function getPolarChart(data, labels, title) {
    return getSimpleChart("polarArea", data, labels, title)
}

generic_charts = ["doughnut", "polarArea"]
generic_plots = ["bar", "bubble", "pie"]
maplot = {
    "table": getTable, "generic": getSimpleChart
}

function plotify(data) {
    for(var i=0; i < data["charts"].length; i++) {
        var chartData = data["charts"][i]
        console.log(chartData)
        var elem = undefined
        if(chartData["type"] == "table") {
            elem = getTable(chartData["data"])
        } else if(generic_plots.indexOf(chartData["type"]) >= 0) {
            elem = makePlotly(chartData["type"], chartData["data"], chartData["layout"])
        } else if(generic_charts.indexOf(chartData["type"]) >= 0) {
            elem = getSimpleChart(chartData["type"], chartData["data"], chartData["labels"], chartData["title"])
        }

        if (elem != undefined) {
            write(elem)
        }
    }
}
