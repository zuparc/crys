function getAdvancedFormData() {
    var form = document.getElementById("advanced-form")
    var data = {}
    for(var i = 0; i < form.children.length; i++) {
        var fields = form.children[i]
        if(!(fields.classList.contains("fields"))) continue
        var fieldsInfo = {}
        for(var j = 0; j < fields.children.length; j++) {
            var field = fields.children[j]
            if(!field.classList.contains("field")) continue
            var fieldPosition = field.getAttribute("position")
            fieldsInfo[fieldPosition] = null
            
            var inputField = field.firstChild
            if(inputField.tagName == undefined) {
                for(var ch = 0; ch < field.children.length; ch++) {
                    var chelem = field.children[ch]
                    if(chelem.tagName != undefined) {
                        inputField = chelem
                        break
                    }
                }
            }
            if(!inputField.tagName) continue
            if(inputField.classList && inputField.classList.contains("dropdown")) {
                var inField = $(inputField)
                fieldsInfo[fieldPosition] = inField.dropdown('get value')
                if(fieldsInfo[fieldPosition] == "") {
                    fieldsInfo[fieldPosition] = inField.dropdown('get text')
                }
            } 
            else if(inputField.tagName.toLowerCase() == "input") {
                var value = inputField.value
                if(inputField.getAttribute("type") == "number") {
                    value = parseInt(value)
                }
                fieldsInfo[fieldPosition] = value
            }
        }
        if(!(fieldsInfo["first"] in data)) data[fieldsInfo["first"]] = {}
        if(!(fieldsInfo["middle"] in data[fieldsInfo["first"]])) data[fieldsInfo["first"]][fieldsInfo["middle"]] = fieldsInfo["last"]
    }
    return data
}


function searchme() {
    // FUNCTION CALLED FOR ADVANCED SEARCH FORM
    var formData = getAdvancedFormData()
    console.log(formData)
    close("adserch")
    toggleLoader()
    request(formData, plotify, true)
}


function getSelectNode(values, multiple=false) {
    var elem = document.createElement("div")
    elem.classList.add("ui", "fluid", "selection", "dropdown")
    var newi = document.createElement("i")
    newi.classList.add("dropdown", "icon")
    elem.appendChild(newi)
    var def = document.createElement("div")
    def.classList.add("default", "text")
    def.innerHTML = values[0]
    elem.appendChild(def)
    var menu = document.createElement("menu")
    menu.classList.add("menu")
    for (var i = 0; i < values.length; i++) {
        var newitem = document.createElement("div")
        newitem.classList.add("item")
        newitem.innerHTML = values[i]
        menu.appendChild(newitem)
    }
    elem.appendChild(menu)
    if (multiple) {
        elem.classList.add("multiple")
    }
    return elem
}


function getInputNode(type="text", placeholder="Input here", value="") {
    var elem = document.createElement("input")
    elem.setAttribute("type", type)
    elem.setAttribute("placeholder", placeholder)
    if(value != ""){
        elem.value = value 
    }
    return elem
}


var advancedOptions = {
    "select-greater": function() {return getSelectNode(["greater than", "lower than", "equal to"])},
    "input-number": function(msg, nr=0) {return getInputNode("number", msg, nr)},
    "date": function() {return getInputNode("text", "insert date", "2000")},
    "input-disabled": function(msg) {
        var elem = getInputNode("text", msg, msg)
//        elem.disabled = true
        elem.setAttribute("readonly", "true")
        return elem
        },
    "select": function(items) {return getSelectNode(items)}
}

var advancedList = {
    "Block reward": {"middle": advancedOptions["select-greater"], "last": function() {return advancedOptions["input-number"]("", 1000000)}},
    "Date founded": {"middle": advancedOptions["select-greater"], "last": advancedOptions["date"]},
    "Proof of Work": {"middle": function() {return advancedOptions["select"](["in", "not in"])}, "last": function() {return getSelectNode(["SHA2-256", "scrypt", "primechain", "Dagger", "ripple"], true)}},
    "Total coins": {"middle": advancedOptions["select-greater"], "last": function() {return advancedOptions["input-number"]("", 1000000)}},
    "Proof of Stake": {"middle": function() {return advancedOptions["input-disabled"]("in")}, "last": function() {return getSelectNode(["interest", "dividend", "faucet", "endorsment"], true)}}
}


function refreshElement(obj) {
    var value = obj.innerHTML
    var option = advancedList[value]
    var formElement = obj.parentNode.parentNode.parentNode.parentNode
    for(var i = 0; i < formElement.childElementCount; i++) {
        var elem = formElement.children[i];
        if(!elem.classList.contains("field")) continue
        var key = elem.getAttribute("position")
        if (!(key in option)) continue
        elem.innerHTML = ""
        elem.appendChild(option[key]())
        if(elem.classList.contains("disabled")) {
            elem.classList.remove("disabled")
        }
        $('.ui.dropdown').dropdown();
    }
}


function addNewFormRow(obj) {
    var parent = obj.parentElement
    var copy = parent.cloneNode(true)
    copy.querySelector("button .close").parentElement.style.display = "block"
    copy.removeChild(copy.querySelector("button .plus").parentElement)
    
    var form = parent.parentElement
    form.appendChild(copy)
    $('.ui.dropdown').dropdown();
    //    var query = document.querySelectorAll('div[class=menu] > div')
    //    for(var i = 0; i < query.length; i++) {
    //        var item = query[i]
    //        
    //        item.addEventListener("click", function(me) {refreshElement(me)})
    //    }
//    obj.remove()
}


function search(obj) {
    if(event.key === 'Enter') {
        var string = obj.value
        toggleLoader()
        request({"search": string}, plotify, true, "/search")
    }
}


// initialize select menu
var selectMenu = document.getElementById("select-menu")
for(var key in advancedList) {
    var newObject = document.createElement("div")
    newObject.classList.add("item")
    newObject.innerHTML = key
    //    newObject.addEventListener("click", function(me) {refreshElement(me)})
    newObject.setAttribute('onclick', 'refreshElement(this)')
    selectMenu.appendChild(newObject)
}
