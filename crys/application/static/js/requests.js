var hostUrl = "http://35.195.9.139"
//var hostUrl = "http://localhost:8001"
var defaultQueryURL = hostUrl + "/advanced_search"
function close(name) {
    document.getElementById(name).classList.remove("active");
    document.getElementById(name + "seg").classList.remove("active");
}


function closeIfActive(name) {
    var obj = document.getElementById(name)
    if(obj.classList.contains("active")) {
        close(name)
    }
}


function togglePlot(obj) {
    var parent = obj.parentElement
    for(var i = 0; i < parent.children.length; i++) {
        var elem = parent.children[i]
        if(!elem.getAttribute("id") && elem.tagName.toLowerCase() != "table") continue
        if(!elem.style.display || elem.style.display == "block") {
//            elem.style.visibility = "hidden"
            elem.style.display = "none"
        } else {
//            elem.style.visibility = "visible"
            elem.style.display = "block"
        }
    }
}

function toggleLoader() {
    var loader = document.getElementById("loader")
    if(loader.classList.contains("active")) {
        loader.classList.remove("active") 
    }
    else {
        loader.classList.add("active")
    }
}

function displayLocalChartGenerator() {
    // display a content box where a modal will open to be able to add more chart from the latest advanced search response data
}


function displayError(text, title="Error occurred") {
    document.querySelector('.message .header').textContent = title
    document.querySelector('.message p').textContent = text
    document.querySelector('.message').classList.remove("hidden")
}


function request(data, callback, store=false, url=defaultQueryURL) {
    var response;
    $.ajax({
        type: "POST",
        //async: false,
        url: url,
        data: JSON.stringify(data),
        contentType: 'application/json',
        success: function(result){
            toggleLoader()
            console.log(data)
            console.log(result)
            if (callback) {callback(result)}
            response = result
            if (store) {  // store latest response for client side chart generation
                advancedData = response
                displayLocalChartGenerator()
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            toggleLoader()
            console.log("Error")
            console.log(textStatus)
            console.log(errorThrown)
            console.log(XMLHttpRequest)
            console.log("XMLHTTPER")
            console.log(XMLHttpRequest.responseText)
            console.log(XMLHttpRequest.status)
            console.log(XMLHttpRequest.statusText)
            
            displayError("[" + XMLHttpRequest.status + "] " + textStatus)
        }
    });
    return response
}
