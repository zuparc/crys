__author__ = 'Robert-Mihail UNGUREANU <roungureanu@bitdefender.com>'

import json
import random
import re

from rdflib import Graph, plugin
from rdflib.serializer import Serializer

from flask import jsonify, request, Response
from flask.views import MethodView

import crys.application
import crys.application.utils
import crys.constants as constants

from crys.sparql_query_executor import SPARQLQueryExecutor
from crys.application.exceptions import InvalidRequest
from crys.application.visualization_plugins import REGISTERED_PLUGINS, PluginManager


class SearchView(MethodView):
    def post(self):
        try:
            json_body = request.get_json() or dict()
        except Exception:
            json_body = dict()

        to_search = json_body.get('search', '').lower()
        coin_ids = list()
        words = re.findall("[A-Za-z0-9]+", to_search)

        for to_search in words:

            query = '''
            SELECT DISTINCT ?coin
            WHERE {{
                ?coin a doacc:Cryptocurrency.
                ?coin ?p ?o
                OPTIONAL {{?o ?pp ?q}}
                FILTER(CONTAINS(LCASE(STR(?o)), "{to_search}") = true || CONTAINS(LCASE(STR(?q)), "{to_search}"))
            }}
            LIMIT 1000
            '''.format(
                to_search=to_search.replace('"', '\\"')
            )

            query_result = SPARQLQueryExecutor().query(query)
            coin_ids += [
                entry['coin']['value'].rsplit('#', 1)[-1] 
                for entry in query_result]

        if len(coin_ids) > 500:
            coin_ids = random.sample(coin_ids, k=500)

        if request.headers.get('Accept') == 'application/ld+json':
            to_convert = dict()

            query = crys.application.utils.build_coins_query(coin_ids)
            query_result = SPARQLQueryExecutor().query_tab_separated_output(query).splitlines()[1:]
            for entry in query_result:
                entries = entry.split('\t')
                if len(entries) == 3:
                    s, p, o = entries
                else:
                    s, p, o, _ = entries

                if (s, p) not in to_convert:
                    to_convert[(s, p)] = list()

                try:
                    int(o)
                    to_convert[(s, p)].append(o)
                    continue
                except Exception:
                    pass

                try:
                    if o.lower() != "nan":
                        float(o)
                        to_convert[(s, p)].append(o)
                        continue
                except Exception:
                    pass

                if o.startswith('<') and o.endswith('>'):
                    to_convert[(s, p)].append(o)
                    continue

                if o.startswith('"') and o.endswith('"'):
                    to_convert[(s, p)].append(o)
                    continue

                if o.startswith('"') and ('@' in o or '^^' in o) and '"' in o[1:]:
                    to_convert[(s, p)].append(o)
                    continue

                o = '"{}"'.format(o)
                to_convert[(s, p)].append(o)

            lines = []
            for (s, p), values in to_convert.items():
                o = ', '.join(values)
                lines.append('{}\t{}\t{} .'.format(s, p, o))

            items = '\n'.join(lines)

            g = Graph().parse(data=items, format='n3')
            output = g.serialize(format='json-ld', indent=4).decode()

            r = Response(response=output, status=200, mimetype="application/xml")
            r.headers["Content-Type"] = "application/ld+json; charset=utf-8"
            return r
        elif request.headers.get('Accept') == 'text/html':
            prefix = 'rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns# ' \
                     'doacc: http://purl.org/net/bel-epa/doacc# ' \
                     'rdfs: http://www.w3.org/2000/01/rdf-schema# ' \
                     'skos: http://www.w3.org/2004/02/skos/core# ' \
                     'dc:  http://purl.org/dc/elements/1.1/ ' \
                     'cce: http://purl.org/net/bel-epa/cce# ' \
                     'geo: http://www.fao.org/countryprofiles/geoinfo/geopolitical/resource/ '

            coin_pattern = '''
            <div vocab="http://purl.org/net/bel-epa/doacc#" 
                prefix="{prefix}" resource="{id}" 
                typeof="Cryptocurrency">
               <span property="block-reward">{block-reward}</span>
               <span property="block-time">{block-time}</span>
               <span property="skos:prefLabel">{prefLabel}</span>
               <span property="dc:description">{description}</span>
               <span property="date-founded">{date-founded}</span>
               <span property="rdfs:comment">{comment}</span>
               <span property="pow">{pow}</span>
               <span property="protection_scheme">{protection_scheme}</span>
               <span property="protocol">{protocol}</span>
               <span property="symbol">{symbol}</span>
               <span property="source">{source}</span>
               <span property="total-coins">{total_coins}</span>
               <span property="website">{website}</span>
            </div>
            '''

            content = ''
            coins = crys.application.utils.retrieve_coins(coin_ids)
            for coin in coins:
                coin['prefix'] = prefix
                for field in {'block-reward', 'block-time', 'prefLabel',
                              'description', 'date-founded', 'comment',
                              'pow', 'protection_scheme', 'protocol',
                              'symbol', 'source', 'total_coins', 'website'}:
                    if field not in coin:
                        coin[field] = ''
                content = content + coin_pattern.format(**coin) + '\n'

            output = '<div>\n' + content + '\n</div>'

            r = Response(response=output, status=200, mimetype="text/html")
            r.headers["Content-Type"] = "application/text/html; charset=utf-8"
            return r
        else:
            coins = crys.application.utils.retrieve_coins(coin_ids)

            charts = []
            for i in REGISTERED_PLUGINS:
                try:
                    charts.append(getattr(PluginManager, i)(coins))
                except Exception as err:
                    print(err, i)

            # response = {
            #     'charts': [PluginManager.plugin_group_by_founded_year(coins)]
            # }

            return jsonify(
                {'charts': charts}), 200, {'Content-Type': 'application/json; charset=utf-8'}
