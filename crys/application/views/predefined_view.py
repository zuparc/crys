import collections

from flask import jsonify, request
from flask.views import MethodView

from crys.sparql_query_executor import SPARQLQueryExecutor


def process_queryid1():
    """
    1: Most recent 30 cryptocurrencies
    :return:
    """
    query = '''
        SELECT DISTINCT ?coin
        WHERE {{
            ?coin a doacc:Cryptocurrency.
            ?coin ?p ?o
            OPTIONAL {{?o ?pp ?q}}
        }}
        LIMIT 200
        '''

    query_result = SPARQLQueryExecutor().query(query)

    coin_ids = [
        entry['coin']['value'].rsplit('#', 1)[-1]
        for entry in query_result
    ]

    query = '''
            SELECT ?coin ?property ?value ?description 
            WHERE {{
                    ?coin a doacc:Cryptocurrency ;
                       ?property ?value .
                OPTIONAL {{ ?value rdfs:label ?description}}
                FILTER (?coin IN ({}))
            }}
        '''.format(
        ', '.join(
            'doacc:' + coin_id
            for coin_id in coin_ids
        )
    )

    query_result = SPARQLQueryExecutor().query(query)

    coins = dict()

    for entry in query_result:
        coin_uri = entry['coin']['value']
        coin_id = coin_uri.rsplit('#', 1)[-1]

        property_uri = entry['property']['value']
        items = property_uri.rsplit('#', 1)
        if len(items) == 1:
            items = property_uri.rsplit('/', 1)
        property_ = items[-1]

        if entry.get('description', dict()).get('value'):
            value = entry['description']['value']
        else:
            value = entry['value']['value']

        if property_ != 'type':
            if coin_id not in coins:
                coins[coin_id] = dict()
            coins[coin_id][property_] = value

    for key in coins:
        if 'coin' in coins[key]:
            coins[key]['id'] = coins[key].pop('coin').rsplit('#', 1)[-1]
        else:
            coins[key]['id'] = key

    headers = [
        ('prefLabel', "Label"),
        ('symbol', "Symbol"),
        ('date-founded', "Date Founded"),
        ('block-reward', "Block Reward"),
        ('total-coins', "Total Coins"),
        ('maturation', "Maturation"),
        ('description', "Description"),
    ]

    floats = ("block-reward", "total-coins", "maturation")

    def checker(x):
        try:
            for i in headers:
                c = x[i[0]] #checks if exists

            if float(x["block-reward"]) \
                    and float(x["block-time"]) \
                    and int(x['total-coins']) < 100000000 \
                    and float(x["block-reward"]) < 2500 \
                    and x['prefLabel']:
                return True
        except:
            pass

        return False

    filtered = list(filter(checker, coins.values()))
    filtered = filtered[:30]

    """
    label,symbol,pow,block reward, block time, total coins, maturation, date-founded, description
    """
    lheaders = [i[0] for i in headers]
    return [{
        'type': 'table',
        'layout': {
            'title': "Most recent 30 Cryptocurrencies",
        },
        'data': [[i[1] for i in headers]] +
                [[i[h] if h not in floats else float(i[h]) for h in lheaders]
                 for i in filtered]
    }]


def process_queryid2():
    """
    2: Cryptocurrencies grouped by year and POW
    :return:
    """
    query = '''
    SELECT DISTINCT ?coin
    WHERE {{
        ?coin a doacc:Cryptocurrency.
        ?coin ?p ?o
        OPTIONAL {{?o ?pp ?q}}
    }}
    LIMIT 200
    '''

    query_result = SPARQLQueryExecutor().query(query)

    coin_ids = [
        entry['coin']['value'].rsplit('#', 1)[-1]
        for entry in query_result
    ]

    query = '''
        SELECT ?coin ?property ?value ?description 
        WHERE {{
                ?coin a doacc:Cryptocurrency ;
                   ?property ?value .
            OPTIONAL {{ ?value rdfs:label ?description}}
            FILTER (?coin IN ({}))
        }}
    '''.format(
        ', '.join(
            'doacc:' + coin_id
            for coin_id in coin_ids
        )
    )

    query_result = SPARQLQueryExecutor().query(query)

    coins = dict()

    for entry in query_result:
        coin_uri = entry['coin']['value']
        coin_id = coin_uri.rsplit('#', 1)[-1]

        property_uri = entry['property']['value']
        items = property_uri.rsplit('#', 1)
        if len(items) == 1:
            items = property_uri.rsplit('/', 1)
        property_ = items[-1]

        if entry.get('description', dict()).get('value'):
            value = entry['description']['value']
        else:
            value = entry['value']['value']

        if property_ != 'type':
            if coin_id not in coins:
                coins[coin_id] = dict()
            coins[coin_id][property_] = value

    for key in coins:
        if 'coin' in coins[key]:
            coins[key]['id'] = coins[key].pop('coin').rsplit('#', 1)[-1]
        else:
            coins[key]['id'] = key

    pows = collections.defaultdict(int)
    for coin in coins.values():
        pow = coin.get("pow", "")
        if not pow or pow.startswith("http"):
            continue

        pows[pow] += 1

    years = collections.defaultdict(int)
    for coin in coins.values():
        year = coin.get("date-founded", "")
        year = year.split("-")[0].split("_")[0]
        if not year:
            continue

        years[year] += 1

    pows = list(sorted(pows.items(), key=lambda x: x[0]))
    years = list(sorted(years.items(), key=lambda x: int(x[0])))

    return [
        {
            'type': "bar",
            'layout': {
                'title': "Number of Cryptocurrencies with same POW",
            },
            'data': [{
                'x': [i[0] for i in pows],
                'y': [i[1] for i in pows],
                'orientation': "v",
            }]
        },
        {
            'type': "bar",
            'layout': {
                'title': "Number of Cryptocurrencies released in same year",
            },
            'data': [{
                'x': [i[1] for i in years],
                'y': [i[0] for i in years],
                'orientation': "h",
            }]
        },
    ]


def process_queryid3():
    """
    3: Top 20 cryptocurrencies grouped by block-time
    :return:
    """
    query = '''
        SELECT DISTINCT ?coin
        WHERE {{
            ?coin a doacc:Cryptocurrency.
            ?coin ?p ?o
            OPTIONAL {{?o ?pp ?q}}
        }}
        LIMIT 200
        '''

    query_result = SPARQLQueryExecutor().query(query)

    coin_ids = [
        entry['coin']['value'].rsplit('#', 1)[-1]
        for entry in query_result
    ]

    query = '''
            SELECT ?coin ?property ?value ?description 
            WHERE {{
                    ?coin a doacc:Cryptocurrency ;
                       ?property ?value .
                OPTIONAL {{ ?value rdfs:label ?description}}
                FILTER (?coin IN ({}))
            }}
        '''.format(
        ', '.join(
            'doacc:' + coin_id
            for coin_id in coin_ids
        )
    )

    query_result = SPARQLQueryExecutor().query(query)

    coins = dict()

    for entry in query_result:
        coin_uri = entry['coin']['value']
        coin_id = coin_uri.rsplit('#', 1)[-1]

        property_uri = entry['property']['value']
        items = property_uri.rsplit('#', 1)
        if len(items) == 1:
            items = property_uri.rsplit('/', 1)
        property_ = items[-1]

        if entry.get('description', dict()).get('value'):
            value = entry['description']['value']
        else:
            value = entry['value']['value']

        if property_ != 'type':
            if coin_id not in coins:
                coins[coin_id] = dict()
            coins[coin_id][property_] = value

    for key in coins:
        if 'coin' in coins[key]:
            coins[key]['id'] = coins[key].pop('coin').rsplit('#', 1)[-1]
        else:
            coins[key]['id'] = key

    def checker(x):
        try:
            if float(x["block-reward"]) \
                    and float(x["block-time"]) \
                    and int(x['total-coins']) < 100000000 \
                    and float(x["block-reward"]) < 2500:
                return True
        except:
            pass

        return False

    filtered = filter(checker, coins.values())
    coins = list(sorted(filtered, key=lambda x: float(x['block-time'])))
    coins = coins[:20]

    br = [float(x['block-reward']) for x in coins]
    tc = [float(x['total-coins']) for x in coins]
    max_tc = max(tc)
    tc = [i * 100 / max_tc for i in tc]
    labels = [x['prefLabel'] for x in coins]

    return [
        {
            'type': "bubble",
            'layout': {
                'title': "Cryptocurrencies ordered by block-reward with respect to total coins",
            },
            'data': [{
                'x': labels,
                'y': br,
                'mode': 'markers',
                'marker': {
                    'size': tc
                }
            }]
        },
    ]


def process_queryid4():
    """
    4: Top 25 cryptocurrencies grouped by maturity
    :return:
    """
    query = '''
        SELECT DISTINCT ?coin
        WHERE {{
            ?coin a doacc:Cryptocurrency.
            ?coin ?p ?o
            OPTIONAL {{?o ?pp ?q}}
        }}
        LIMIT 500
        '''

    query_result = SPARQLQueryExecutor().query(query)

    coin_ids = [
        entry['coin']['value'].rsplit('#', 1)[-1]
        for entry in query_result
    ]

    query = '''
            SELECT ?coin ?property ?value ?description 
            WHERE {{
                    ?coin a doacc:Cryptocurrency ;
                       ?property ?value .
                OPTIONAL {{ ?value rdfs:label ?description}}
                FILTER (?coin IN ({}))
            }}
        '''.format(
        ', '.join(
            'doacc:' + coin_id
            for coin_id in coin_ids
        )
    )

    query_result = SPARQLQueryExecutor().query(query)

    coins = dict()

    for entry in query_result:
        coin_uri = entry['coin']['value']
        coin_id = coin_uri.rsplit('#', 1)[-1]

        property_uri = entry['property']['value']
        items = property_uri.rsplit('#', 1)
        if len(items) == 1:
            items = property_uri.rsplit('/', 1)
        property_ = items[-1]

        if entry.get('description', dict()).get('value'):
            value = entry['description']['value']
        else:
            value = entry['value']['value']

        if property_ != 'type':
            if coin_id not in coins:
                coins[coin_id] = dict()
            coins[coin_id][property_] = value

    for key in coins:
        if 'coin' in coins[key]:
            coins[key]['id'] = coins[key].pop('coin').rsplit('#', 1)[-1]
        else:
            coins[key]['id'] = key

    def checker(x):
        try:
            if int(x['total-coins']) < 100000000 \
                    and int(x['maturation']) and x['premine'] != "non":
                return True
        except:
            pass

        return False

    def mapper(x):
        try:
            if "%" in x['premine']:
                premine = float(x['premine'].split("%")[0])
                if premine > 100 or premine > 0:
                    return None
                x['premine'] = (premine * x["total-coins"]) // 100

            if float(x['premine']) >= int(x['total-coins']):
                return None
            return float(x['premine']), int(x['total-coins']), \
                   int(x['maturation']), x['prefLabel']
        except Exception as e:
            print(e, x)
            return None

    filtered = filter(checker, coins.values())
    mapped = [i for i in map(mapper, filtered) if i]
    mapped = mapped[:25]

    return [
        {
            'type': "bar",
            'layout': {
                'title': "Coins maturity and premine considering total coins",
            },
            'data': [
                {
                    "x": [i[3] for i in mapped],
                    "y": [100 for _ in mapped],
                    "name": "Total Coins"
                },
                {
                    "x": [i[3] for i in mapped],
                    "y": [i[0] * 100 / i[1] for i in mapped],
                    "name": "Premined"
                },
                {
                    "x": [i[3] for i in mapped],
                    "y": [-i[2] for i in mapped],
                    "name": "Maturity"
                },
            ]
        },
    ]


QUERIES = {
    1: process_queryid1,
    2: process_queryid2,
    3: process_queryid3,
    4: process_queryid4,
}


class PredefinedView(MethodView):
    def post(self):
        try:
            json_body = request.get_json() or dict()
        except Exception:
            json_body = dict()

        queryid = int(json_body.get('queryid', "1"))
        data = QUERIES[queryid]()

        return jsonify({
            'charts': data
        })
