from flask import jsonify
from flask.views import MethodView

from crys.application.visualization_plugins import REGISTERED_PLUGINS

class VisualizationPluginsView(MethodView):

    @staticmethod
    def get():
        return jsonify(REGISTERED_PLUGINS)
