__author__ = 'Robert-Mihail UNGUREANU <roungureanu@bitdefender.com>'

import jinja2
import json

from rdflib import Graph, plugin
from rdflib.serializer import Serializer

from flask import jsonify, request, Response
from flask.views import MethodView

import crys.application
import crys.application.utils
import crys.constants as constants

from crys.sparql_query_executor import SPARQLQueryExecutor
from crys.sparql_query_creator import SPARQLQueryCreator

from crys.application.exceptions import InvalidRequest

from crys.application.visualization_plugins import PluginManager, REGISTERED_PLUGINS


class AdvancedSearchView(MethodView):
    def post(self):
        try:
            request_data = request.get_json() or dict()
        except Exception:
            request_data = dict()

        operators_mapping = {
            'greater than': '>',
            'lower than': '<',
            'equal to': '=',
            'in': 'in',
            'not in': 'not in'
        }

        fields_mapping = {
            'block reward': 'block-reward',
            'proof of work': 'pow',
            'Proof of Work': 'pow',
            'date founded': 'date-founded',
            'running': 'running',
            'total coins': 'total-coins',
            'proof of stake': 'pos'
        }

        filters = []
        for field, field_info in request_data.items():
            for operator, value in field_info.items():
                if operator == 'in':
                    value = value.split(',')
                filters.append(
                    {
                        'field': fields_mapping[field],
                        'operator': operators_mapping[operator],
                        'value': value
                    }
                )

        query = SPARQLQueryCreator.create(
            filters
        )
        print(query)

        query_result = SPARQLQueryExecutor().query(query)

        coin_ids = [
            entry['coin']['value'].rsplit('#', 1)[-1]
            for entry in query_result
        ]

        if request.headers.get('Accept') == 'application/ld+json':
            to_convert = dict()

            query = crys.application.utils.build_coins_query(coin_ids)
            query_result = SPARQLQueryExecutor().query_tab_separated_output(query).splitlines()[1:]
            for entry in query_result:
                entries = entry.split('\t')
                if len(entries) == 3:
                    s, p, o = entries
                else:
                    s, p, o, _ = entries

                if (s, p) not in to_convert:
                    to_convert[(s, p)] = list()

                try:
                    int(o)
                    to_convert[(s, p)].append(o)
                    continue
                except Exception:
                    pass

                try:
                    if o.lower() != "nan":
                        float(o)
                        to_convert[(s, p)].append(o)
                        continue
                except Exception:
                    pass

                if o.startswith('<') and o.endswith('>'):
                    to_convert[(s, p)].append(o)
                    continue

                if o.startswith('"') and o.endswith('"'):
                    to_convert[(s, p)].append(o)
                    continue

                if o.startswith('"') and ('@' in o or '^^' in o) and '"' in o[1:]:
                    to_convert[(s, p)].append(o)
                    continue

                o = '"{}"'.format(o)
                to_convert[(s, p)].append(o)

            lines = []
            for (s, p), values in to_convert.items():
                o = ', '.join(values)
                lines.append('{}\t{}\t{} .'.format(s, p, o))

            items = '\n'.join(lines)

            g = Graph().parse(data=items, format='n3')
            output = g.serialize(format='json-ld', indent=4).decode()

            r = Response(response=output, status=200, mimetype="application/xml")
            r.headers["Content-Type"] = "application/ld+json; charset=utf-8"
            return r
        elif request.headers.get('Accept') == 'text/html':
            prefix = 'rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns# ' \
                     'doacc: http://purl.org/net/bel-epa/doacc# ' \
                     'rdfs: http://www.w3.org/2000/01/rdf-schema# ' \
                     'skos: http://www.w3.org/2004/02/skos/core# ' \
                     'dc:  http://purl.org/dc/elements/1.1/ ' \
                     'cce: http://purl.org/net/bel-epa/cce# ' \
                     'geo: http://www.fao.org/countryprofiles/geoinfo/geopolitical/resource/ '

            coin_pattern = '''
            <div vocab="http://purl.org/net/bel-epa/doacc#" 
                prefix="{prefix}" resource="{id}" 
                typeof="Cryptocurrency">
               <span property="block-reward">{block-reward}</span>
               <span property="block-time">{block-time}</span>
               <span property="skos:prefLabel">{prefLabel}</span>
               <span property="dc:description">{description}</span>
               <span property="date-founded">{date-founded}</span>
               <span property="rdfs:comment">{comment}</span>
               <span property="pow">{pow}</span>
               <span property="protection_scheme">{protection_scheme}</span>
               <span property="protocol">{protocol}</span>
               <span property="symbol">{symbol}</span>
               <span property="source">{source}</span>
               <span property="total-coins">{total_coins}</span>
               <span property="website">{website}</span>
            </div>
            '''

            content = ''
            coins = crys.application.utils.retrieve_coins(coin_ids)
            for coin in coins:
                coin['prefix'] = prefix
                for field in {'block-reward', 'block-time', 'prefLabel',
                              'description', 'date-founded', 'comment',
                              'pow', 'protection_scheme', 'protocol',
                              'symbol', 'source', 'total_coins', 'website'}:
                    if field not in coin:
                        coin[field] = ''
                content = content + coin_pattern.format(**coin) + '\n'

            output = '<div>\n' + content + '\n</div>'

            r = Response(response=output, status=200, mimetype="text/html")
            r.headers["Content-Type"] = "application/text/html; charset=utf-8"
            return r
        else:
            coins = crys.application.utils.retrieve_coins(coin_ids)

            charts = []
            for i in REGISTERED_PLUGINS:
                try:
                    charts.append(getattr(PluginManager, i)(coins))
                except Exception as err:
                    print(err, i)

            response = {
                'charts': charts
            }

            return jsonify(response), 200, {'Content-Type': 'application/json; charset=utf-8'}
