__author__ = 'Robert-Mihail UNGUREANU <roungureanu@bitdefender.com>'

import uuid
import jinja2
from flask import render_template, request, jsonify
from flask.views import MethodView

import crys.application
import crys.constants as constants

from crys.application.decorators import requires_authorization
from crys.application.exceptions import InvalidRequest
from crys.sparql_query_executor import SPARQLQueryExecutor


class CryptoCurrenciesExchangeView(MethodView):
    def get(self, exchange_id=None):
        if exchange_id:
            query = '''
                SELECT ?property ?value ?description 
                WHERE {{ 
                    cce:{} a cce:CryptocurrencyExchange ;
                             ?property ?value .
                    OPTIONAL {{ ?value rdfs:label ?description}}
                }}
            '''.format(exchange_id)
            query_result = SPARQLQueryExecutor().query(query)

            exchange = dict()

            for entry in query_result:
                property_uri = entry['property']['value']
                items = property_uri.rsplit('#', 1)
                if len(items) == 1:
                    items = property_uri.rsplit('/', 1)
                property_ = items[-1]

                if entry.get('description', dict()).get('value'):
                    value = entry['description']['value']
                else:
                    value = entry['value']['value']

                if property_ != 'type':
                    exchange[property_] = value

            if exchange:
                exchange['id'] = exchange_id

            if 'exchange' in exchange:
                exchange.pop('exchange')

            return jsonify(exchange)
        else:
            try:
                limit = int(request.args.get('limit', 50))
            except Exception:
                limit = 50
            try:
                offset = int(request.args.get('offset', 0))
            except Exception:
                offset = 0

            exchanges_ids_query = '''
                SELECT ?id
                WHERE {{ 
                    ?id a cce:CryptocurrencyExchange .
                }}
                ORDER BY ASC(?id)
                LIMIT {}
                OFFSET {}
            '''.format(limit, offset)
            query_result = SPARQLQueryExecutor().query(exchanges_ids_query)

            exchanges_ids = [
                entry['id']['value'].rsplit('#', 1)[-1]
                for entry in query_result
            ]

            query = '''
                SELECT ?exchange ?property ?value ?description 
                WHERE {{
                        ?exchange a cce:CryptocurrencyExchange ;
                           ?property ?value .
                    OPTIONAL {{ ?value rdfs:label ?description}}
                    FILTER (?exchange IN ({}))
                }}
            '''.format(
                ', '.join(
                    'cce:' + exchange_id
                    for exchange_id in exchanges_ids
                )
            )
            query_result = SPARQLQueryExecutor().query(query)

            coins = dict()

            for entry in query_result:
                exchange_uri = entry['exchange']['value']
                exchange_id = exchange_uri.rsplit('#', 1)[-1]

                property_uri = entry['property']['value']
                items = property_uri.rsplit('#', 1)
                if len(items) == 1:
                    items = property_uri.rsplit('/', 1)
                property_ = items[-1]

                if entry.get('description', dict()).get('value'):
                    value = entry['description']['value']
                else:
                    value = entry['value']['value']

                try:
                    value = int(value)
                except Exception:
                    try:
                        value = float(value)
                    except Exception:
                        if value.lower() in {"true", "false"}:
                            value = bool(value)

                if property_ != 'type':
                    if exchange_id not in coins:
                        coins[exchange_id] = dict()
                    coins[exchange_id][property_] = value

            for key in coins:
                if 'exchange' in coins[key]:
                    coins[key]['id'] = coins[key].pop('exchange').rsplit('#', 1)[-1]
                else:
                    coins[key]['id'] = key

            return jsonify(list(coins.values()))

    @staticmethod
    def validate_exchange(exchange):
        conditions = [
            {
                'condition': isinstance(exchange.get('codeISO2'), str),
                'message': '"codeISO2" must be a string.'
            },
            {
                'condition': isinstance(exchange.get('countryprofile'), str),
                'message': '"countryprofile" must be a string.'
            },
            {
                'condition': isinstance(exchange.get('fiat'), bool),
                'message': '"fiat" must be a boolean.'
            },
            {
                'condition': isinstance(exchange.get('voting'), str),
                'message': '"voting" must be a string.'
            },
            {
                'condition': isinstance(exchange.get('page'), str),
                'message': '"page" must be a string.'
            },
            {
                'condition': isinstance(exchange.get('prefLabel'), str),
                'message': '"prefLabel" must be a string.'
            },
            {
                'condition': isinstance(exchange.get('title'), str),
                'message': '"title" must be a string.'
            },
            {
                'condition': isinstance(exchange.get('api'), str),
                'message': '"api" must be a string.'
            },
            {
                'condition': isinstance(exchange.get('merchant'), bool),
                'message': '"merchant" must be a boolean.'
            },
            {
                'condition': isinstance(exchange.get('extant'), bool),
                'message': '"extant" must be a boolean.'
            }
        ]

        for item in conditions:
            if not item['condition']:
                return item['message']

        return True

    @requires_authorization
    def post(self, exchange_id=None):
        try:
            exchange = request.get_json() or dict()
        except Exception:
            exchange = dict()

        validation_result = self.validate_exchange(exchange)
        if validation_result is True:
            exchange['id'] = uuid.uuid4().hex

            for key, value in exchange.items():
                if isinstance(value, str) and key != 'id':
                    value = exchange[key].replace('"', '\\"')
                    exchange[key] = '"{}"'.format(value)
                if isinstance(value, bool):
                    exchange[key] = str(exchange[key]).lower()

            query = '''
                INSERT DATA
                {{ 
                    cce:{id} rdf:type cce:CryptocurrencyExchange ;
                    cce:extant {extant} ;
                    dc:title {title} ;
                    cce:countryprofile {countryprofile} ;
                    geo:codeISO2 {codeISO2} ;
                    cce:api {api} ;
                    cce:voting {voting} ;
                    cce:fiat {fiat} ;
                    cce:merchant {merchant} ;
                    <http://daml.umbc.edu/ontologies/cobra/0.4/foaf/page> {page};
                    skos:prefLabel {prefLabel} .
                }}
            '''.format(**exchange)
            SPARQLQueryExecutor().update_query(query)
            return jsonify(exchange), 201
        else:
            return jsonify(
                {
                    'message': validation_result
                }
            ), 400

    @requires_authorization
    def put(self, exchange_id=None):
        try:
            exchange = request.get_json() or dict()
        except Exception:
            exchange = dict()

        validation_result = self.validate_exchange(exchange)
        if validation_result is True:
            exchange['id'] = exchange_id

            for key, value in exchange.items():
                if isinstance(value, str) and key != 'id':
                    value = exchange[key].replace('"', '\\"')
                    exchange[key] = '"{}"'.format(value)
                if isinstance(value, bool):
                    exchange[key] = str(exchange[key]).lower()

            query = '''
                DELETE
                {{
                    cce:{id} ?p ?o
                }}
                INSERT
                {{ 
                    cce:{id} rdf:type cce:CryptocurrencyExchange ;
                    cce:extant {extant} ;
                    dc:title {title} ;
                    cce:countryprofile {countryprofile} ;
                    geo:codeISO2 {codeISO2} ;
                    cce:api {api} ;
                    cce:voting {voting} ;
                    cce:fiat {fiat} ;
                    cce:merchant {merchant} ;
                    <http://daml.umbc.edu/ontologies/cobra/0.4/foaf/page> {page};
                    skos:prefLabel {prefLabel} .
                }}
                WHERE {{
                    OPTIONAL {{
                        cce:{id} ?p ?o
                    }}
                }}
            '''.format(**exchange)
            SPARQLQueryExecutor().update_query(query)

            return jsonify(exchange), 200
        else:
            return jsonify(
                {
                    'message': validation_result
                }, 400
            )

    @requires_authorization
    def delete(self, exchange_id=None):
        query = '''
            DELETE
            {{ 
                cce:{id} ?p ?o 
            }}
            WHERE
            {{
                cce:{id} ?p ?o
            }}
        '''.format(id=exchange_id)
        SPARQLQueryExecutor().update_query(query)

        return jsonify(
            {
                'message': 'Deleted {}'.format(exchange_id)
            }
        ), 200
