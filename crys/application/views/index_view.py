__author__ = 'Robert-Mihail UNGUREANU <roungureanu@bitdefender.com>'

import jinja2
from flask import render_template
from flask.views import MethodView

import crys.application
import crys.constants as constants

from crys.application.exceptions import InvalidRequest


class IndexView(MethodView):
    def get(self):
        return render_template('index.html')
