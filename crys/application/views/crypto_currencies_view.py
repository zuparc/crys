__author__ = 'Robert-Mihail UNGUREANU <roungureanu@bitdefender.com>'

import uuid
import jinja2
from flask import render_template, request, jsonify
from flask.views import MethodView

import crys.application
import crys.application.utils
import crys.constants as constants

from crys.application.decorators import requires_authorization
from crys.application.exceptions import InvalidRequest
from crys.sparql_query_executor import SPARQLQueryExecutor


class CryptoCurrenciesView(MethodView):
    def get(self, coin_id=None):
        if coin_id:
            coin_ids = [coin_id]

            coins = crys.application.utils.retrieve_coins(coin_ids)
            if coins:
                coin = coins[0]
            else:
                coin = dict()

            return jsonify(coin)
        else:
            try:
                limit = int(request.args.get('limit', 50))
            except Exception:
                limit = 50
            try:
                offset = int(request.args.get('offset', 0))
            except Exception:
                offset = 0

            coin_ids_query = '''
                SELECT ?id
                WHERE {{ 
                    ?id a doacc:Cryptocurrency .
                }}
                ORDER BY ASC(?id)
                LIMIT {}
                OFFSET {}
            '''.format(limit, offset)
            query_result = SPARQLQueryExecutor().query(coin_ids_query)

            coin_ids = [
                entry['id']['value'].rsplit('#', 1)[-1]
                for entry in query_result
            ]

            coins = crys.application.utils.retrieve_coins(coin_ids)

            return jsonify(coins)

    @staticmethod
    def validate_coin(coin):
        conditions = [
            {
                'condition': isinstance(coin.get('block_reward'), int),
                'message': '"block_reward" must be an integer.'
            },
            {
                'condition': isinstance(coin.get('block_time'), int),
                'message': '"block_time" must be an integer.'
            },
            {
                'condition': isinstance(coin.get('comment'), str),
                'message': '"comment" must be a string.'
            },
            {
                'condition': isinstance(coin.get('date_founded'), str),
                'message': '"date_founded" must be a string.'
            },
            {
                'condition': isinstance(coin.get('distribution_scheme'), str),
                'message': '"distribution_scheme" must be a string.'
            },
            {
                'condition': isinstance(coin.get('expiration'), str),
                'message': '"expiration" must be a string.'
            },
            {
                'condition': isinstance(coin.get('incept'), str),
                'message': '"incept" must be a string.'
            },
            {
                'condition': isinstance(coin.get('pow'), str),
                'message': '"pow" must be a string.'
            },
            {
                'condition': isinstance(coin.get('protection_scheme'), str),
                'message': '"protection_scheme" must be a string.'
            },
            {
                'condition': isinstance(coin.get('protocol'), str),
                'message': '"protocol" must be a string.'
            },
            {
                'condition': isinstance(coin.get('source'), str),
                'message': '"source" must be a string.'
            },
            {
                'condition': isinstance(coin.get('symbol'), str),
                'message': '"symbol" must be a string.'
            },
            {
                'condition': isinstance(coin.get('total_coins'), int),
                'message': '"total_coins" must be an integer.'
            },
            {
                'condition': isinstance(coin.get('website'), str),
                'message': '"website" must be a string.'
            },
            {
                'condition': isinstance(coin.get('pref_label'), str),
                'message': '"pref_label" must be a string.'
            }
        ]

        for item in conditions:
            if not item['condition']:
                return item['message']

        return True

    @requires_authorization
    def post(self, coin_id=None):
        try:
            coin = request.get_json() or dict()
        except Exception:
            coin = dict()

        validation_result = self.validate_coin(coin)
        if validation_result is True:
            coin['id'] = uuid.uuid4().hex

            original_protocol = coin['protocol']
            original_pow = coin['pow']
            for key, value in coin.items():
                if key in {'protocol', 'pow'}:
                    value = crys.application.utils.retrieve_object_for_property(
                        'doacc:' + key, value
                    )
                    if not value:
                        possible_values = crys.application.utils.retrieve_possible_values_for_property('doacc:' + key)
                        possible_values = ', '.join(possible_values)
                        return jsonify(
                            {
                                "message": 'Invalid value for key "{}". Possible values: {}'.format(
                                    key, possible_values
                                )
                            }
                        ), 400
                    else:
                        coin[key] = '<{}>'.format(value)
                else:
                    if isinstance(value, str) and key != 'id':
                        value = coin[key].replace('"', '\\"')
                        coin[key] = '"{}"'.format(value)
                    if isinstance(value, bool):
                        coin[key] = str(coin[key]).lower()

            query = '''
                INSERT DATA
                {{ 
                    doacc:{id} rdf:type doacc:Cryptocurrency ;
                    dc:description {description} ;
                    doacc:date-founded {date_founded} ;
                    rdfs:comment {comment} ;
                    doacc:pow {pow} ;
                    doacc:protection_scheme {protection_scheme} ;
                    doacc:protocol {protocol} ;
                    doacc:source {source} ;
                    doacc:symbol {symbol} ;
                    doacc:total-coins {total_coins} ;
                    doacc:website {website} ;
                    skos:prefLabel {pref_label} ;
                    doacc:block-reward {block_reward} ;
                    doacc:block-time {block_time} .
                }}
            '''.format(**coin)
            SPARQLQueryExecutor().update_query(query)

            coin['protocol'] = original_protocol
            coin['pow'] = original_pow
            return jsonify(coin), 201
        else:
            return jsonify(
                {
                    'message': validation_result
                }
            ), 400

    @requires_authorization
    def put(self, coin_id=None):
        try:
            coin = request.get_json() or dict()
        except Exception:
            coin = dict()

        validation_result = self.validate_coin(coin)
        if validation_result is True:
            coin['id'] = coin_id

            original_protocol = coin['protocol']
            original_pow = coin['pow']
            for key, value in coin.items():
                if key in {'protocol', 'pow'}:
                    value = crys.application.utils.retrieve_object_for_property(
                        'doacc:' + key, value
                    )
                    possible_values = crys.application.utils.retrieve_possible_values_for_property('doacc:' + key)
                    possible_values = ', '.join(possible_values)
                    if not value:
                        return jsonify(
                            {
                                "message": 'Invalid value for key "{}". Possible values: {}'.format(
                                    key, possible_values
                                )
                            }
                        ), 400
                    else:
                        coin[key] = '<{}>'.format(value)
                else:
                    if isinstance(value, str) and key != 'id':
                        value = coin[key].replace('"', '\\"')
                        coin[key] = '"{}"'.format(value)
                    if isinstance(value, bool):
                        coin[key] = str(coin[key]).lower()

            query = '''
                DELETE
                {{
                    doacc:{id} ?p ?o
                }}
                INSERT
                {{ 
                    doacc:{id} rdf:type doacc:Cryptocurrency ;
                    dc:description {description} ;
                    doacc:date-founded {date_founded} ;
                    rdfs:comment {comment} ;
                    doacc:pow {pow} ;
                    doacc:protection_scheme {protection_scheme} ;
                    doacc:protocol {protocol} ;
                    doacc:source {source} ;
                    doacc:symbol {symbol} ;
                    doacc:total-coins {total_coins} ;
                    doacc:website {website} ;
                    skos:prefLabel {pref_label} ;
                    doacc:block-reward {block_reward} ;
                    doacc:block-time {block_time} .
                }}
                WHERE {{
                    OPTIONAL {{
                        doacc:{id} ?p ?o
                    }}
                }}
            '''.format(**coin)
            SPARQLQueryExecutor().update_query(query)

            coin['protocol'] = original_protocol
            coin['pow'] = original_pow
            return jsonify(coin), 200
        else:
            return jsonify(
                {
                    'message': validation_result
                }, 400
            )

    @requires_authorization
    def delete(self, coin_id=None):
        query = '''
            DELETE
            {{ 
                doacc:{id} ?p ?o 
            }}
            WHERE
            {{
                doacc:{id} ?p ?o
            }}
        '''.format(id=coin_id)
        SPARQLQueryExecutor().update_query(query)

        return jsonify(
            {
                'message': 'Deleted {}'.format(coin_id)
            }
        ), 200
