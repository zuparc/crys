import jinja2

from flask import Flask, jsonify

from crys.constants import TEMPLATES_FOLDER
from crys.application.exceptions import InvalidRequest
from crys.application.views.index_view import IndexView
from crys.application.views.search_view import SearchView
from crys.application.views.advanced_search_view import AdvancedSearchView
from crys.application.views.crypto_currencies_view import CryptoCurrenciesView
from crys.application.views.cryptocurrencies_exchange_view import CryptoCurrenciesExchangeView
from crys.application.views.visualization_plugins_view import \
    VisualizationPluginsView
from crys.application.views.predefined_view import PredefinedView

app = Flask(__name__)

jinja_context = jinja2.Environment(
    loader=jinja2.PackageLoader('crys.application', TEMPLATES_FOLDER)
)

app.config['JSON_SORT_KEYS'] = True

index_view = IndexView.as_view('index')
search_view = SearchView.as_view('search')
advanced_search_view = AdvancedSearchView.as_view('advanced_search')
cryptocurrencies_view = CryptoCurrenciesView.as_view('cryptoccurrencies')
cryptocurrencies_exchange_view = CryptoCurrenciesExchangeView.as_view('cryptocurrencies_exchange')
visualization_plugins_view = VisualizationPluginsView.as_view(
    'visualization_plugins')
predefined_view = PredefinedView.as_view('predefined')


# === Application handlers ===
@app.errorhandler(InvalidRequest)
def handle_bad_request(error):
    response = jsonify({'message': error.get_message()})
    response.status_code = error.get_status_code()
    return response


# === Routes ===
app.add_url_rule(
    endpoint='index',
    rule="/",
    view_func=index_view,
    methods=['GET'],
)

app.add_url_rule(
    endpoint='search',
    rule="/search",
    view_func=search_view,
    methods=['POST'],
)

app.add_url_rule(
    endpoint='advanced_search',
    rule="/advanced_search",
    view_func=advanced_search_view,
    methods=['POST'],
)

app.add_url_rule(
    endpoint='cryptocurrencies',
    rule='/api/cryptocurrencies/',
    view_func=cryptocurrencies_view,
    methods=['GET', 'POST'],
    defaults={
        'coin_id': None
    }
)

app.add_url_rule(
    endpoint='cryptocurrencies_single',
    rule='/api/cryptocurrencies/<string:coin_id>',
    view_func=cryptocurrencies_view,
    methods=['GET', 'PUT', 'DELETE'],
)

app.add_url_rule(
    endpoint='cryptocurrencies-exchange',
    rule='/api/cryptocurrencies-exchanges/',
    view_func=cryptocurrencies_exchange_view,
    methods=['GET', 'POST'],
    defaults={
        'exchange_id': None
    }
)

app.add_url_rule(
    endpoint='cryptocurrencies-exchange-single',
    rule='/api/cryptocurrencies-exchanges/<string:exchange_id>',
    view_func=cryptocurrencies_exchange_view,
    methods=['GET', 'PUT', 'DELETE'],
)

app.add_url_rule(
    endpoint='visualization_plugins',
    rule='/api/visualization_plugins/',
    view_func=visualization_plugins_view,
    methods=['GET'],
)

app.add_url_rule(
    endpoint='predefined',
    rule='/predefined/',
    view_func=predefined_view,
    methods=['GET', "POST"],
)
