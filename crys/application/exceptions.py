__author__ = 'Robert-Mihail UNGUREANU <roungureanu@bitdefender.com>'


class BasicException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class InvalidRequest(Exception):
    def __init__(self, message, status_code=400, traceback=""):
        self.message = message
        self.status_code = status_code
        self.traceback = traceback

    def get_message(self):
        return self.message

    def get_status_code(self):
        return self.status_code

    def get_traceback(self):
        return self.traceback
