import os

ROOT_PATH = os.path.dirname(  # crys root folder
    os.path.dirname(  # crys/crys package
        os.path.abspath(__file__)  # crys/crys/constants.py
    )
)

TEMPLATES_FOLDER = os.path.join(ROOT_PATH, 'crys', 'application', 'templates')


class Templates:
    index = os.path.join(TEMPLATES_FOLDER, 'index.html')


SPARQL_ENDPOINT_QUERY_URL = 'http://35.239.207.221/repositories/crys-1'
SPARQL_ENDPOINT_UPDATE_URL = 'http://35.239.207.221/repositories/crys-1/statements'
SPARQL_ENDPOINT_USER = 'wade'
SPARQL_ENDPOINT_PASSWORD = 'P@ssw0rd'


SERVER = {
    'HOST': '0.0.0.0',
    'PORT': 8001
}

SECRET = '150df4bc42627c316e68950a7e8ecc4019aec471d30d659b2818b0fd3d92002202e5df539' \
         '54382e5efb4b1624cfd29b086a4a851736a3c488806817a4347db2c5486ea013c1e45136a' \
         'c14ee2dbe3c0c209957d56b0888a10395e1afd9f523c4e14103a4009a95cd83e4f5e451b6' \
         'b0be7bb8df2f5d3b0e7945ca378757a060d631557746eb98c953c828b6f8484a98c322418' \
         'a179ff69e6487cc69812eeff5361db74b114559efc41ea048ee872ac8abd5d749d6607024' \
         '40412ad029c429ea26130e3abf6ca61c48d0952c65d56de279db4fa6ce8ebf9c3f880dc21' \
         '97a2ee8445b66e5d0ef294d88e804a80a486760828ea9024d69602bec41dc917ebe0a6488c'
