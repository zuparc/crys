# README #

WADE Project


Technical Report can be found in docs/index.html. The technical report consists of the previous report but a new section was added, Implemented Solution.

Technical Guide can be found in docs/user_guide.html

The movie for the project is the movie.mp4 file.



Short explanation of some of the project's files and what they are used for:

crys\sparql_query_creator.py - component for sending SPARQL queries to the RDF storage

crys\sparql_query_executor.py - component for creating SPARQL queries from filters supplied in Advanced Search page

crys\application\visualization_plugins.py - plugins used in backend to generate data for rendering charts.

crys\application\views\search_view.py - component for simple search

crys\application\views\advanced_search_view.py - component for advanced search

crys\application\views\crypto_currencies_view.py - REST API routes for Cryptocurrencies

crys\application\views\predefined_view.py - SPARQL Queries for predefined views

crys\application\templates\index.html and scripts found in crys\application\static\js - Interface alongside logic for rendering charts based on web server response.